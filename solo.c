#include <X11/Xlib.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#define PI 3.14159265
#define LADOS 12
#define ASTEROIDES 50
#define ANCHO 800
#define ALTO 600

struct Punto {
    int x, y;
    double r;
} Asteroides[ASTEROIDES], Puntos[LADOS];

void setColor(XColor color, Display* display, int R, int G, int B)
{
    color.red = R;
    color.green = G;
    color.blue = B;
    XAllocColor(display, DefaultColormap(display, DefaultScreen(display)),
        &color);
    XSetForeground(display, XDefaultGC(display, DefaultScreen(display)),
        color.pixel);
}

int crearRandom(int min, int max) { return rand() % (max + 1 - min) + min; }
int crearRadio()
{
    return crearRandom(15, 60); // Crea un radio aleatorio de 15 a 60
}

double alargamiento()
{
    int x = crearRandom(0, 20);
    int neg = crearRandom(0, 1);
    if (neg == 0)
        return -x;
    return x;
}

double dist(int x1, int y1, int x2, int y2)
{
    double x = (x1 - x2) * (x1 - x2);
    double y = (y1 - y2) * (y1 - y2);
    return sqrt(x + y);
}

int main(void)
{
    srand(time(NULL));
    Display* display;
    Window window;
    XEvent event;
    XColor color;
    color.flags = DoRed | DoGreen | DoBlue;
    int s, x, y, i, j, x1, x2, y1, y2;

    display = XOpenDisplay(NULL);
    s = DefaultScreen(display);

    window = XCreateSimpleWindow(display, RootWindow(display, s), 10, 10, ANCHO, ALTO,
        1, BlackPixel(display, s), WhitePixel(display, s));
    XSelectInput(display, window, ExposureMask | KeyPressMask);
    XMapWindow(display, window);

    for (;;) {
        XNextEvent(display, &event);
        if (event.type == Expose) {
            // Dibujar Fondo
            setColor(color, display, 0, 0, 0);
            XFillRectangle(display, window, DefaultGC(display, s), 0, 0, ANCHO, ALTO);

            for (i = 0; i < ASTEROIDES; i++) {
                x = crearRandom(0, ANCHO);
                y = crearRandom(0, ALTO); // crear centro asteroide
                double radio = crearRadio();
                int pasa = 1;
                if (x - radio * 1.2 < 0 || x + radio * 1.2 > ANCHO || y - radio * 1.2 < 0 || y + radio * 1.2 > ALTO)
                    pasa = 0;

                for (j = 0; j < i; j++) {
                    x1 = Asteroides[j].x;
                    y1 = Asteroides[j].y;
                    double d = dist(x, y, x1, y1);
                    if (d < Asteroides[j].r * 1.2 + radio * 1.2)
                        pasa = 0;
                }
                if (pasa == 0) {
                    i--;
                    continue;
                }
                Asteroides[i].x = x;
                Asteroides[i].y = y;
                Asteroides[i].r = radio;

                setColor(color, display, 65535, 65535, 65535);
                XDrawLine(display, window, XDefaultGC(display, DefaultScreen(display)),
                    x, y, x, y);

                int ang, angMax = 360 / LADOS, angMin = 0, cont = 0;
                double radio2;
                for (cont = 0; cont < LADOS;
                     cont++, angMin += 360 / LADOS, angMax += 360 / LADOS) {
                    ang = crearRandom(angMin, angMax);
                    printf("%d %d\n", angMin, angMax);
                    radio2 = radio * (1.0 + alargamiento() / 100);

                    x1 = (int)(radio2 * cos(ang * PI / 180));
                    y1 = (int)(radio2 * sin(ang * PI / 180));

                    Puntos[cont].x = x + x1;
                    Puntos[cont].y = y + y1;
                }
                setColor(color, display, crearRandom(0, 65535), crearRandom(0, 65535),
                    crearRandom(0, 65535));
                for (cont = 0; cont < LADOS - 1; cont++) {
                    x1 = Puntos[cont].x;
                    y1 = Puntos[cont].y;

                    x2 = Puntos[cont + 1].x;
                    y2 = Puntos[cont + 1].y;
                    XDrawLine(display, window,
                        XDefaultGC(display, DefaultScreen(display)), x1, y1, x2,
                        y2);
                }

                x1 = Puntos[LADOS - 1].x;
                y1 = Puntos[LADOS - 1].y;
                x2 = Puntos[0].x;
                y2 = Puntos[0].y;
                XDrawLine(display, window, XDefaultGC(display, DefaultScreen(display)),
                    x1, y1, x2, y2);
            }
        }
        if (event.type == KeyPress)
            break;
    }
    /* close connection to server */
    XCloseDisplay(display);
    return 0;
}