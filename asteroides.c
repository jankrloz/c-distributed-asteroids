#include <X11/Xlib.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define GRANULARIDAD 12
#define ASTEROIDES 50
#define AGUDEZ 20
#define ANCHO 800
#define ALTO 600

struct PuntosAsteroide {
    int x, y;
} Puntos[GRANULARIDAD];

void setColor(XColor color, Display* display, int R, int G, int B)
{
    color.red = R;
    color.green = G;
    color.blue = B;
    XAllocColor(display, DefaultColormap(display, DefaultScreen(display)), &color);
    XSetForeground(display, XDefaultGC(display, DefaultScreen(display)), color.pixel);
}

int generarRandom(int min, int max) { return rand() % (max + 1 - min) + min; }
int generarRadio() { return generarRandom(15, 60); }

double aumentoRadio()
{
    int x = generarRandom(0, AGUDEZ);
    int neg = generarRandom(0, 1);
    if (neg == 0)
        return -x;
    return x;
}

int main(void)
{
    srand(time(NULL));
    Display* display;
    Window window;
    XEvent event;
    XColor color;
    int s, centro_x, centro_y, i, x1, x2, y1, y2;

    display = XOpenDisplay(NULL);
    s = DefaultScreen(display);

    window = XCreateSimpleWindow(display, RootWindow(display, s), 10, 10, ANCHO, ALTO, 1, BlackPixel(display, s), WhitePixel(display, s));
    XSelectInput(display, window, ExposureMask | KeyPressMask);
    XMapWindow(display, window);

    while (1) {
        XNextEvent(display, &event);
        if (event.type == Expose) {
            // Dibujar Fondo
            setColor(color, display, 0, 0, 0);
            XFillRectangle(display, window, DefaultGC(display, s), 0, 0, ANCHO, ALTO);

            // Ciclo para generar cada asteroide
            for (i = 0; i < ASTEROIDES; i++) {

                // Centro y radio de asteroide aleatorio
                centro_x = generarRandom(0, ANCHO);
                centro_y = generarRandom(0, ALTO);
                double radio = generarRadio();

                // Dibujamos el centro
                setColor(color, display, 65535, 65535, 65535);
                // XDrawLine(display, window, XDefaultGC(display, DefaultScreen(display)), x, y, x, y);

                int ang, angMax = 360 / GRANULARIDAD, angMin = 0, cont = 0;
                double radio2;

                // Ciclo para generar los puntos del asteroide i
                for (cont = 0; cont < GRANULARIDAD; cont++, angMin += 360 / GRANULARIDAD, angMax += 360 / GRANULARIDAD) {

                    // Generamos el angulo aleatorio en el rango valido
                    ang = generarRandom(angMin, angMax);
                    radio2 = radio * (1.0 + aumentoRadio() / 100);

                    x1 = (int)(radio2 * cos(ang * M_PI / 180));
                    y1 = (int)(radio2 * sin(ang * M_PI / 180));

                    Puntos[cont].x = centro_x + x1;
                    Puntos[cont].y = centro_y + y1;
                }

                // Ciclo para dibujar los lados del asteroide i
                setColor(color, display, 65535, 65535, 65535);
                for (cont = 0; cont < GRANULARIDAD - 1; cont++) {
                    x1 = Puntos[cont].x;
                    y1 = Puntos[cont].y;

                    x2 = Puntos[cont + 1].x;
                    y2 = Puntos[cont + 1].y;
                    XDrawLine(display, window, XDefaultGC(display, DefaultScreen(display)), x1, y1, x2, y2);
                }

                // Cerramos el asteroide
                x1 = Puntos[GRANULARIDAD - 1].x;
                y1 = Puntos[GRANULARIDAD - 1].y;
                x2 = Puntos[0].x;
                y2 = Puntos[0].y;
                XDrawLine(display, window, XDefaultGC(display, DefaultScreen(display)), x1, y1, x2, y2);
            }
        }
        if (event.type == KeyPress)
            break;
    }
    
    XCloseDisplay(display);
    return 0;
}