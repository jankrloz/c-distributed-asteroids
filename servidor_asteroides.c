#include <X11/Xlib.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#define GRANULARIDAD 12
#define ANCHO 800
#define ALTO 600

struct PuntoAsteroide {
  int x, y;
} Punto[GRANULARIDAD];

void setColor(XColor color, Display *display, int R, int G, int B) {
  color.red = R;
  color.green = G;
  color.blue = B;
  XAllocColor(display, DefaultColormap(display, DefaultScreen(display)),
              &color);
  XSetForeground(display, XDefaultGC(display, DefaultScreen(display)),
                 color.pixel);
}

int puerto = 7200;

int main(int argc, char *argv[]) {

  // inicializacion de UDP
  int s, server_socket, i, clilen;
  struct sockaddr_in server_addr, msg_to_client_addr;

  server_socket = socket(AF_INET, SOCK_DGRAM, 0);

  /* se asigna una direccion al socket del servidor*/
  bzero((char *)&server_addr, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  server_addr.sin_port = htons(puerto);

  bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr));
  clilen = sizeof(msg_to_client_addr);

  // Configuracion e inicializacion de X11

  Display *display;
  Window window;
  XEvent event;
  XColor color;

  display = XOpenDisplay(NULL);
  s = DefaultScreen(display);

  window =
      XCreateSimpleWindow(display, RootWindow(display, s), 10, 10, ANCHO, ALTO,
                          1, BlackPixel(display, s), WhitePixel(display, s));
  XSelectInput(display, window, ExposureMask | KeyPressMask);
  XMapWindow(display, window);

  while (1) {
    XNextEvent(display, &event);
    if (event.type == Expose) {
      // Dibujar Fondo
      setColor(color, display, 0, 0, 0);
      XFillRectangle(display, window, DefaultGC(display, s), 0, 0, ANCHO, ALTO);

      // Comenzamos a recibir los Punto
      while (1) {
        recvfrom(server_socket, (char *)Punto, GRANULARIDAD * sizeof(Punto), 0,
                 (struct sockaddr *)&msg_to_client_addr, (u_int *)&clilen);
        printf("Llego conexion\n");

        printf("El puerto es: %d \n", ntohs(msg_to_client_addr.sin_port));

        for (i = 0; i < GRANULARIDAD; i++) {
          printf("%d, %d\n", Punto[i].x, Punto[i].y);
        }

        int cont, x1, x2, y1, y2;

        // Ciclo para dibujar los lados del asteroide i
        setColor(color, display, 65535, 65535, 65535);
        for (cont = 0; cont < GRANULARIDAD - 1; cont++) {
          x1 = Punto[cont].x;
          y1 = Punto[cont].y;

          x2 = Punto[cont + 1].x;
          y2 = Punto[cont + 1].y;
          XDrawLine(display, window,
                    XDefaultGC(display, DefaultScreen(display)), x1, y1, x2,
                    y2);
        }

        // Cerramos el asteroide
        x1 = Punto[GRANULARIDAD - 1].x;
        y1 = Punto[GRANULARIDAD - 1].y;
        x2 = Punto[0].x;
        y2 = Punto[0].y;
        XDrawLine(display, window, XDefaultGC(display, DefaultScreen(display)),
                  x1, y1, x2, y2);
      }
    }
    if (event.type == KeyPress)
      break;
  }

  XCloseDisplay(display);
}
