#include <arpa/inet.h>
#include <math.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define GRANULARIDAD 12
#define ASTEROIDES 50
#define AGUDEZ 20
#define ANCHO 800
#define ALTO 600

struct PuntoAsteroide {
  int x, y;
} Punto[GRANULARIDAD];

int generarRandom(int min, int max) { return rand() % (max + 1 - min) + min; }
int generarRadio() { return generarRandom(15, 60); }

double aumentoRadio() {
  int x = generarRandom(0, AGUDEZ);
  int neg = generarRandom(0, 1);
  if (neg == 0)
    return -x;
  return x;
}

int puerto = 7200;

int main(int argc, char *argv[]) {
  struct sockaddr_in msg_to_server_addr, client_addr;
  int client_socket;

  client_socket = socket(AF_INET, SOCK_DGRAM, INADDR_ANY);

  /* rellena la dirección del servidor */
  bzero((char *)&msg_to_server_addr, sizeof(msg_to_server_addr));

  msg_to_server_addr.sin_family = AF_INET;
  msg_to_server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  msg_to_server_addr.sin_port = htons(puerto);

  printf("Conectando a servidor en localhost:%d\n", puerto);

  /* rellena la direcciòn del cliente*/
  bzero((char *)&client_addr, sizeof(client_addr));
  client_addr.sin_family = AF_INET;
  client_addr.sin_addr.s_addr = INADDR_ANY;
  client_addr.sin_port = htons(0);

  bind(client_socket, (struct sockaddr *)&client_addr, sizeof(client_addr));

  // Aqui comienza la generacion de los asteroides
  srand(time(NULL));
  int centro_x, centro_y, i, x1, y1;

  // Ciclo para generar cada asteroide
  for (i = 0;; i++) {

    // Centro y radio de asteroide aleatorio
    centro_x = generarRandom(0, ANCHO);
    centro_y = generarRandom(0, ALTO);
    double radio = generarRadio();

    int ang, angMax = 360 / GRANULARIDAD, angMin = 0, cont = 0;
    double radio2;

    // Ciclo para generar los Punto del asteroide i
    for (cont = 0; cont < GRANULARIDAD;
         cont++, angMin += 360 / GRANULARIDAD, angMax += 360 / GRANULARIDAD) {

      // Generamos el angulo aleatorio en el rango valido
      ang = generarRandom(angMin, angMax);
      radio2 = radio * (1.0 + aumentoRadio() / 100);

      x1 = (int)(radio2 * cos(ang * M_PI / 180));
      y1 = (int)(radio2 * sin(ang * M_PI / 180));

      Punto[cont].x = centro_x + x1;
      Punto[cont].y = centro_y + y1;
    } // Fin de ciclo de creacion de Punto

    // Los Punto del asteroide están contenidos en Punto[GRANULARIDAD]
    printf("\n***** Asteroide %d *****\n", i + 1);
    for (int cont = 0; cont < GRANULARIDAD; cont++) {
      printf("Punto[%d] = (%d, %d)\n", cont, Punto[cont].x, Punto[cont].y);
    }

    printf("Enviando mensaje ...\n");
    sendto(client_socket, (char *)Punto, GRANULARIDAD * 2 * sizeof(int), 0,
           (struct sockaddr *)&msg_to_server_addr, sizeof(msg_to_server_addr));

    sleep(1);

  } // Fin de ciclo de asteroides

  close(client_socket);
  return 0;
}
